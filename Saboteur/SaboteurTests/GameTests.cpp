#include "stdafx.h"
#include "CppUnitTest.h"

#include "Game.h"
#include "Player.h"
#include "Board.h"



using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace SaboteurTests
{
	TEST_CLASS(GameTests) 
	{
	public:

		TEST_METHOD(Distribuire) {
			Board board;
			Game game;
			std::vector<Player>listOfPlayers;
			listOfPlayers.insert(listOfPlayers.end(),4, Player());
			
			listOfPlayers[0].setRole(DwarfCard(DwarfCard::Type::GoldDigger));
			listOfPlayers[1].setRole(DwarfCard(DwarfCard::Type::GoldDigger));
			listOfPlayers[2].setRole(DwarfCard(DwarfCard::Type::Saboteur));
			listOfPlayers[3].setRole(DwarfCard(DwarfCard::Type::GoldDigger));
			game.setListOfPlayers(listOfPlayers);
			game.setNumberOfPlayers();
			game.generateGoldCards();

			std::vector<Player>::iterator iterator = listOfPlayers.begin();
			for (iterator; iterator != listOfPlayers.end(); ++iterator)
				//if (BoardStateChecker::Check(board) == BoardStateChecker::State::GoldDiggerWin)
					game.distributeGold(false, *iterator);
				//else
					//game.distributeGold(false, *iterator);
			Assert::IsTrue(listOfPlayers[2].getScore() != 0);
		}
		

	};
}