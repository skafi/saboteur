#include "stdafx.h"
#include "CppUnitTest.h"

#include"ActionCard.h"
#include"BeginCard.h"
#include"PathCard.h"
#include"GoldCard.h"
#include"DwarfCard.h"



using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace SaboteurTests
{		
	TEST_CLASS(CardTests)
	{
	public:
		
		TEST_METHOD(DefaultConstructorActionCard)
		{
			ActionCard card(ActionCard::Type::FixPickaxe);
			Assert::IsTrue(ActionCard::Type::FixPickaxe==card.getType());
		}
		
		TEST_METHOD(DefaultConstructorPathCard)
		{
			PathCard card(PathCard::Type::DownAndLeft);
			Assert::IsTrue(PathCard::Type::DownAndLeft==card.getType());
			Assert::IsTrue(card.getEast()== false);
			Assert::IsTrue(card.getNorth()== false);
			Assert::IsTrue(card.getSouth()==true);
			Assert::IsTrue(card.getWest()==true);
			Assert::IsTrue(card.getCenter()==true);
		}

		TEST_METHOD(DefaultConstructorBeginCard)
		{
			BeginCard card(BeginCard::Type::Ladder);
			Assert::IsTrue(BeginCard::Type::Ladder ==card.getType());
		}

		TEST_METHOD(DefaultConstructorDwarfCard)
		{
			DwarfCard card(DwarfCard::Type::Saboteur);
			Assert::IsTrue(DwarfCard::Type::Saboteur ==card.getType());
		}

		TEST_METHOD(DefaultConstructorGoldCard)
		{
			GoldCard card(GoldCard::Type::OnePieceGold);
			Assert::IsTrue(GoldCard::Type::OnePieceGold ==card.getType());
		}

	};
}