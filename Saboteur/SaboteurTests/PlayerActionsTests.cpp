#include "stdafx.h"
#include "CppUnitTest.h"

#include"BeginCard.h"
#include"PathCard.h"
#include"Board.h"
#include"ActionCard.h"
#include"Player.h"
#include"BoardStateChecker.h"



using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace SaboteurTests
{
	TEST_CLASS(PlayerActionsTests)
	{
	public:
		TEST_METHOD(BlockAPlayer){
		
			Player player;
			ActionCard card(ActionCard::Type::BreakCart);
			player.blockPlayer(player, card);

			Assert::IsTrue(player.getStatus()==true);
			Assert::IsTrue(player.getCardsThatBlockMe()[0]==card);
		}

		TEST_METHOD(UnblockAPlayer) {

			Player player;
			ActionCard card(ActionCard::Type::BreakCart);
			player.blockPlayer(player, card);
	
			card=ActionCard(ActionCard::Type::FixCart);
			player.unblockPlayer(player, card);

			Assert::IsTrue(player.getStatus() == false);
			Assert::IsTrue(player.getCardsThatBlockMe().empty()==true);			
		}

		TEST_METHOD(DestroyAPathCard) {

			Board board;
			Player player;
			ActionCard destroyPath(ActionCard::Type::DestroyPath);
			Board::Position position = {2,2};
			board[{ 2, 2 }] = std::variant<PathCard, BeginCard>(PathCard::Type::Junction);

			player.destroyPathCard(board, position);

			Assert::IsTrue(board[{ 2, 2 }].has_value()==false);
		}

		TEST_METHOD(PlaceAPathCard) {

			Board board;
			Player player;
			PathCard card(PathCard::Type::BlindAlleyLeftAndUp);
			Board::Position position = { 2,2 };
			board[{ 2, 2 }] = std::variant<PathCard, BeginCard>(PathCard::Type::Junction);
						
			Assert::Assert::ExpectException<const char*>([&]() {
				player.placePathCard(card, 2, 2, board);
			});;
		}

	};
}

