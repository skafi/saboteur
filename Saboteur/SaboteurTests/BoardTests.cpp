#include "stdafx.h"
#include "CppUnitTest.h"

#include"BeginCard.h"
#include"PathCard.h"
#include"Board.h"



using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace SaboteurTests
{
	TEST_CLASS(BoardTests)
	{
	public:

		TEST_METHOD(SetGetOneOne)
		{
			Board board;
			std::variant<PathCard, BeginCard> path;
			const Board::Position position = { 1, 1 };
			board[position] = path;

			Assert::IsTrue(board[position].has_value());
		}

		TEST_METHOD(GetAtOneOneConst)
		{
			const Board board;
			const Board::Position position{ 1, 1 };

			Assert::IsFalse(board[position].has_value());
		}

		TEST_METHOD(GetAtMinusOneOne)
		{
			Board board;
			const Board::Position position{ -1, 1 };

			Assert::ExpectException<std::out_of_range>([&]() {
				board[position];
			});
		}

		TEST_METHOD(GetAtMinusOneOneConst)
		{
			const Board board;
			const Board::Position position{ -1, 1 };

			Assert::ExpectException<std::out_of_range>([&]() {
				board[position];
			});
		}

		TEST_METHOD(GetAtOneMinusOne)
		{
			Board board;
			const Board::Position position{ 1, -1 };

			Assert::ExpectException<std::out_of_range>([&]() {
				board[position];
			});
		}

		TEST_METHOD(GetAtOneMinusOneConst)
		{
			const Board board;
			Board::Position position{ 1, -1 };

			Assert::ExpectException<std::out_of_range>([&]() {
				board[position];
			});
		}

		TEST_METHOD(GetAtOneMaxWidth)
		{
			Board board;
			const Board::Position position{ 1, Board::getBoardWidth() };

			Assert::ExpectException<std::out_of_range>([&]() {
				board[position];
			});
		}
		
		TEST_METHOD(GetAtOneMaxWidthConst)
		{
			const Board board;
			const Board::Position position{ 1,  Board::getBoardWidth() };

			Assert::ExpectException<std::out_of_range>([&]() {
				board[position];
			});
		}
		
		TEST_METHOD(GetAtMaxHeightOne)
		{
			Board board;
			const Board::Position position{ Board::getBoardHeight(), 1 };

			Assert::ExpectException<std::out_of_range>([&]() {
				board[position];
			});
		}
		TEST_METHOD(GetAtMaxHeightOneConst)
		{
			const Board board;
			const Board::Position position{ Board::getBoardHeight(), 1 };

			Assert::ExpectException<std::out_of_range>([&]() {
				board[position];
			});
		}

	};
}

