#include "stdafx.h"
#include "CppUnitTest.h"

#include"BeginCard.h"
#include"PathCard.h"
#include"Board.h"
#include"BoardStateChecker.h"



using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace SaboteurTests
{
	TEST_CLASS(BoardStateCheckerTests)
	{
	public:
		TEST_METHOD(PathBetweenTwoCards)
		{
			Board board;
			Board::Position beginPosition = { 8, 2 };
			board[beginPosition] = std::variant<PathCard, BeginCard>(BeginCard::Type::Ladder);
			board[{ 7, 2 }] = std::variant<PathCard, BeginCard>(PathCard::Type::Junction);
			board[{ 6, 2 }] = std::variant<PathCard, BeginCard>(PathCard::Type::Junction);
			board[{ 5, 2 }] = std::variant<PathCard, BeginCard>(PathCard::Type::UpAndDown);
			board[{ 7, 1 }] = std::variant<PathCard, BeginCard>(PathCard::Type::DownAndRight);
			board[{ 7, 3 }] = std::variant<PathCard, BeginCard>(PathCard::Type::UpAndLeft);
			board[{ 6, 3 }] = std::variant<PathCard, BeginCard>(PathCard::Type::UpDownAndLeft);
			board[{ 5, 3 }] = std::variant<PathCard, BeginCard>(PathCard::Type::UpAndDown);
			Board::Position endPosition = { 4, 3 };
			board[endPosition] = std::variant<PathCard, BeginCard>(BeginCard::Type::Gold) ;

			Assert::IsTrue(BoardStateChecker::isPath(beginPosition, endPosition, board));

		}

		TEST_METHOD(CheckState)
		{
			Board board;
			board[{ 8, 2 }] = std::variant<PathCard, BeginCard>(BeginCard::Type::Ladder);

			board[{ 7, 2 }] = std::variant<PathCard, BeginCard>(PathCard::Type::Junction);
			board[{ 6, 2 }] = std::variant<PathCard, BeginCard>(PathCard::Type::Junction);
			board[{ 5, 2 }] = std::variant<PathCard, BeginCard>(PathCard::Type::Junction);
			board[{ 4, 2 }] = std::variant<PathCard, BeginCard>(PathCard::Type::Junction);
			board[{ 3, 2 }] = std::variant<PathCard, BeginCard>(PathCard::Type::Junction);
			board[{ 2, 2 }] = std::variant<PathCard, BeginCard>(PathCard::Type::Junction);
			board[{ 1, 2 }] = std::variant<PathCard, BeginCard>(PathCard::Type::Junction);
			
			board[{ 2, 1 }] = std::variant<PathCard, BeginCard>(PathCard::Type::Junction);
			board[{ 2, 0 }] = std::variant<PathCard, BeginCard>(PathCard::Type::Junction);
			board[{ 1, 0 }] = std::variant<PathCard, BeginCard>(PathCard::Type::Junction);


			board[{ 0, 0 }] = std::variant<PathCard, BeginCard>(BeginCard::Type::Rock);
			board[{ 0, 2 }] = std::variant<PathCard, BeginCard>(BeginCard::Type::Gold);
			board[{ 0, 4 }] = std::variant<PathCard, BeginCard>(BeginCard::Type::Rock);

			Assert::IsTrue(BoardStateChecker::Check(board)== BoardStateChecker::State::GoldDiggerWin);
		}

	};
}

