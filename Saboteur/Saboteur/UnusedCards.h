#pragma once
#include "ActionCard.h"
#include "PathCard.h"

#include <variant>
#include <vector>
class UnusedCards
{
public:
	std::vector<std::variant<PathCard, ActionCard>>m_cardPack;

	UnusedCards() = default;

	UnusedCards(const UnusedCards &other);
	UnusedCards(UnusedCards &&other);

	UnusedCards & operator = (const UnusedCards &other);
	UnusedCards & operator = (UnusedCards &&other);

	void generateCardPack();
};

