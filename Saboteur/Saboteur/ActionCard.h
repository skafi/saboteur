#pragma once
#include<iostream>
class ActionCard
{
public:
	//type of cards which represents an action
	enum class Type : uint8_t
	{
		None,
		BreakLamp,
		BreakCart,
		BreakPickaxe,
		FixLamp,
		FixCart,
		FixPickaxe,
		FixPickaxeOrLamp,
		FixPickaxeOrCart,
		FixLampOrCart,
		View,
		DestroyPath
	};

	ActionCard();
	ActionCard(const Type& type);
	ActionCard(const ActionCard &other);
	ActionCard(ActionCard &&other);
	~ActionCard() = default;

	ActionCard & operator = (const ActionCard &other);
	ActionCard & operator = (ActionCard &&other);
	bool operator==(const ActionCard& other) const;

	Type getType() const;

	friend std::ostream& operator << (std::ostream& os, const ActionCard& actionCard);

private:
	Type m_type : 4;
};

