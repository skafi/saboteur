#pragma once
#include "Board.h"
#include "Player.h"
#include "PathCard.h"
#include "ActionCard.h"

#include<iostream>
class Display
{
public:
	static void displayBoard(const Board& board);
	static void displayPlayerCards(Player player);
	static void displayPlayerNameAndStatus(Player player);
	static void Display::displayGameName();
	static void Display::displayImage(const std::string& fileName);
};

