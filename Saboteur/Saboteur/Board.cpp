#include "Board.h"

const char kEmptyBoardCell = 219;

const std::optional<std::variant<PathCard, BeginCard>>& Board::operator[](const Position& position) const
{
	const auto& line = position.first;
	const auto& column = position.second;

	if (line >= kHeight || column >= kWidth)
		throw std::out_of_range("Board index out of bound.");

	return m_cards[line * kWidth + column];
}

std::optional<std::variant<PathCard, BeginCard>>& Board::operator[](const Position& position)
{
	const auto& line = position.first;
	const auto& column = position.second;

	if (line >= kHeight || column >= kWidth)
		throw std::out_of_range("Board index out of bound.");

	return m_cards[line * kWidth + column];
}

const short Board::getBoardWidth()
{
	return kWidth;
}

const short Board::getBoardHeight()
{
	return kHeight;
}

std::ostream & operator<<(std::ostream & os, const Board & board)
{
	Board::Position position;
	auto& line = position.first;
	auto& column = position.second;

	for (line = 0; line < Board::kHeight; ++line)
	{
		for (column = 0; column < Board::kWidth; ++column)
		{
			if (board[position])
			{
				if (std::holds_alternative<PathCard>(*board[position]))
				{
					const PathCard card = std::get<PathCard>(*board[position]);
					os << card;
				}
				else
					if (std::holds_alternative<BeginCard>(*board[position]))
					{
						const BeginCard card = std::get<BeginCard>(*board[position]);
						os << card;
					}
			}
			else
				for (int index = 0; index < 3; ++index)
					os << kEmptyBoardCell;
		}
		os << std::endl;
	}

	return os;
}
