#include "GoldCard.h"

GoldCard::GoldCard(const Type& type)
	: m_type(type)
{
	//EMPTY
}

GoldCard::GoldCard(const GoldCard & other)
{
	*this = other;
}

GoldCard::GoldCard(GoldCard && other)
{
	*this = std::move(other);
}

GoldCard & GoldCard::operator=(const GoldCard & other)
{
	m_type = other.m_type;
	return *this;
}

GoldCard & GoldCard::operator=(GoldCard && other)
{
	m_type = other.m_type;

	new(&other) GoldCard;

	return *this;
}

bool GoldCard::operator<(const GoldCard & other) const
{
	return static_cast<short>(m_type) < static_cast<short>(other.m_type);
}

GoldCard::Type GoldCard::getType() const
{
	return m_type;
}

std::ostream & operator<<(std::ostream & os, const GoldCard & goldCard)
{
	return os << static_cast<int>(goldCard.m_type);
}
