#pragma once
#include<iostream>
class BeginCard
{
public:
	enum class Type : uint8_t
	{
		Ladder,
		Rock,
		Gold
	};
	BeginCard() = default;
	BeginCard(const Type& type);
	BeginCard(const BeginCard &other);
	BeginCard(BeginCard &&other);
	~BeginCard() = default;

	BeginCard & operator = (const BeginCard &other);
	BeginCard & operator = (BeginCard &&other);

	Type getType() const;

	friend std::ostream& operator << (std::ostream& os, const BeginCard& beginCards);

private:
	Type m_type : 3;
};

