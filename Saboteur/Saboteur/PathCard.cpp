﻿#include "PathCard.h"

PathCard::PathCard() : PathCard(Type::None)
{
	//EMPTY
}

PathCard::PathCard(const Type & type) : m_type(type)
{
	switch (m_type)
	{
	case Type::Junction:
		m_north = true;
		m_south = true;
		m_center = true;
		m_east = true;
		m_west = true;
		break;
	case Type::BlindAlleyJunction:
		m_north = true;
		m_south = true;
		m_east = true;
		m_west = true;
		break;
	case Type::UpAndDown:
		m_north = true;
		m_south = true;
		m_center = true;
		break;
	case Type::BlindAlleyUpAndDown:
		m_north = true;
		m_south = true;
		break;
	case Type::LeftAndRight:
		m_center = true;
		m_east = true;
		m_west = true;
		break;
	case Type::BlindAlleyLeftAndRight:
		m_east = true;
		m_west = true;
		break;
	case Type::BlindAlleyLeft:
		m_west = true;
		break;
	case Type::BlindAlleyRight:
		m_east = true;
		break;
	case Type::BlindAlleyUp:
		m_north = true;
		break;
	case Type::BlindAlleyDown:
		m_south = true;
		break;
	case Type::DownAndLeft:
		m_south = true;
		m_center = true;
		m_west = true;
		break;
	case Type::BlindAlleyLeftAndDown:
		m_south = true;
		m_west = true;
		break;
	case Type::UpAndRight:
		m_north = true;
		m_center = true;
		m_east = true;
		break;
	case Type::BlindAlleyRightAndUp:
		m_north = true;
		m_east = true;
		break;
	case Type::UpAndLeft:
		m_north = true;
		m_center = true;
		m_west = true;
		break;
	case Type::BlindAlleyLeftAndUp:
		m_north = true;
		m_west = true;
		break;
	case Type::DownAndRight:
		m_south = true;
		m_center = true;
		m_east = true;
		break;
	case Type::BlindAlleyRightAndDown:
		m_south = true;
		m_east = true;
		break;
	case Type::LeftRightAndUp:
		m_north = true;
		m_center = true;
		m_east = true;
		m_west = true;
		break;
	case Type::BlindAlleyLeftRightAndUp:
		m_north = true;
		m_east = true;
		m_west = true;
		break;
	case Type::LeftRightAndDown:
		m_south = true;
		m_center = true;
		m_east = true;
		m_west = true;
		break;
	case Type::BlindAlleyLeftRightAndDown:
		m_south = true;
		m_east = true;
		m_west = true;
		break;
	case Type::UpDownAndLeft:
		m_north = true;
		m_south = true;
		m_center = true;
		m_west = true;
		break;
	case Type::BlindAlleyLeftUpAndDown:
		m_north = true;
		m_south = true;
		m_west = true;
		break;
	case Type::BlindAlleyRightUpAndDown:
		m_north = true;
		m_south = true;
		m_east = true;
		break;
	case Type::UpDownAndRight:
		m_north = true;
		m_south = true;
		m_center = true;
		m_east = true;
		break;
	}
}

PathCard::PathCard(const PathCard & other)
{
	*this = other;
}

PathCard::PathCard(PathCard && other)
{
	*this = std::move(other);
}

PathCard::Type PathCard::getType() const
{
	return m_type;
}

bool PathCard::getNorth() const
{
	return m_north;
}

bool PathCard::getSouth() const
{
	return m_south;
}

bool PathCard::getEast() const
{
	return m_east;
}

bool PathCard::getWest() const
{
	return m_west;
}

bool PathCard::getCenter() const
{
	return m_center;
}

PathCard & PathCard::operator=(const PathCard & other)
{
	m_type = other.m_type;
	m_north = other.m_north;
	m_center = other.m_center;
	m_south = other.m_south;
	m_east = other.m_east;
	m_west = other.m_west;

	return *this;
}

PathCard & PathCard::operator=(PathCard && other)
{
	m_type = other.m_type;
	m_north = other.m_north;
	m_center = other.m_center;
	m_south = other.m_south;
	m_east = other.m_east;
	m_west = other.m_west;
	new(&other) PathCard;

	return *this;
}

bool PathCard::operator==(const PathCard & other) const
{
	if (m_type == other.m_type)
		return true;
	return false;
}

PathCard PathCard::rotateCard(const PathCard & card) const
{
	switch (card.m_type)
	{
	case Type::BlindAlleyDown:
		return PathCard(Type::BlindAlleyUp);
		break;
	case Type::BlindAlleyUp:
		return PathCard(Type::BlindAlleyDown);
		break;
	case Type::BlindAlleyLeft:
		return PathCard(Type::BlindAlleyRight);
		break;
	case Type::BlindAlleyRight:
		return PathCard(Type::BlindAlleyLeft);
		break;
	case Type::DownAndLeft:
		return PathCard(Type::UpAndRight);
		break;
	case Type::UpAndRight:
		return PathCard(Type::DownAndLeft);
		break;
	case Type::BlindAlleyRightAndUp:
		return PathCard(Type::BlindAlleyLeftAndDown);
		break;
	case Type::BlindAlleyLeftAndDown:
		return PathCard(Type::BlindAlleyRightAndUp);
		break;
	case Type::DownAndRight:
		return PathCard(Type::UpAndLeft);
		break;
	case Type::UpAndLeft:
		return PathCard(Type::DownAndRight);
		break;
	case Type::BlindAlleyLeftAndUp:
		return PathCard(Type::BlindAlleyRightAndDown);
		break;
	case Type::BlindAlleyRightAndDown:
		return PathCard(Type::BlindAlleyLeftAndUp);
		break;
	case Type::BlindAlleyLeftRightAndDown:
		return PathCard(Type::BlindAlleyLeftAndUp);
		break;
	case Type::LeftRightAndUp:
		return PathCard(Type::LeftRightAndDown);
		break;
	case Type::LeftRightAndDown:
		return PathCard(Type::LeftRightAndUp);
		break;
	case Type::UpDownAndLeft:
		return PathCard(Type::UpDownAndRight);
		break;
	case Type::UpDownAndRight:
		return PathCard(Type::UpDownAndLeft);
		break;
	case Type::BlindAlleyLeftUpAndDown:
		return PathCard(Type::BlindAlleyRightUpAndDown);
		break;
	case Type::BlindAlleyRightUpAndDown:
		return PathCard(Type::BlindAlleyLeftUpAndDown);
		break;
	default:
		throw "THIS CARD CAN'T BE ROTATED";
	}
}

std::ostream & operator<<(std::ostream & os, const PathCard & pathCard)
{
	char character1 = '?', character2 = '?', character3 = '?';
	switch (pathCard.m_type)
	{
	case PathCard::Type::Junction:
		character1 = 205;//═
		character2 = 206;//╬
		character3 = 205;//═
		break;
	case PathCard::Type::UpAndDown:
		character1 = 219;//█
		character2 = 186;//║
		character3 = 219;//█
		break;
	case PathCard::Type::LeftAndRight:
		character1 = 205;//═
		character2 = 205;//═
		character3 = 205;//═
		break;
	case PathCard::Type::UpAndLeft:
		character1 = 205;//═
		character2 = 188;//╝
		character3 = 219;//█
		break;
	case PathCard::Type::DownAndRight:
		character1 = 219;//█
		character2 = 201;//╔
		character3 = 205;//═
		break;
	case PathCard::Type::UpAndRight:
		character1 = 219;//█
		character2 = 200;//╚
		character3 = 205;//═
		break;
	case PathCard::Type::DownAndLeft:
		character1 = 205;//═
		character2 = 187;//╗
		character3 = 219;//█
		break;
	case PathCard::Type::UpDownAndLeft:
		character1 = 205;//═
		character2 = 185;//╣
		character3 = 219;//█
		break;
	case PathCard::Type::UpDownAndRight:
		character1 = 219;//█
		character2 = 204;//╠
		character3 = 205;//═
		break;
	case PathCard::Type::LeftRightAndDown:
		character1 = 205;//═
		character2 = 203;//╦
		character3 = 205;//═
		break;
	case PathCard::Type::LeftRightAndUp:
		character1 = 205;//═
		character2 = 202;//╩
		character3 = 205;//═
		break;
	case PathCard::Type::BlindAlleyLeft:
		character1 = 205;//═
		character2 = 205;//═
		character3 = 219;//█
		break;
	case PathCard::Type::BlindAlleyRight:
		character1 = 219;//█
		character2 = 205;//═
		character3 = 205;//═
		break;
	case PathCard::Type::BlindAlleyUp:
		character1 = 219;//█
		character2 = 208;//╨
		character3 = 219;//█
		break;
	case PathCard::Type::BlindAlleyDown:
		character1 = 219;//█
		character2 = 210;//╥
		character3 = 219;//█
		break;
	case PathCard::Type::BlindAlleyJunction:
		character1 = 181;//╡
		character2 = 215;//╫
		character3 = 198;//╞
		break;
	case PathCard::Type::BlindAlleyUpAndDown:
		character1 = 219;//█
		character2 = 215;//╫
		character3 = 219;//█
		break;
	case PathCard::Type::BlindAlleyLeftAndRight:
		character1 = 181;//╡
		character2 = 219;//█
		character3 = 198;//╞
		break;
	case PathCard::Type::BlindAlleyLeftAndDown:
		character1 = 181;//╡
		character2 = 210;//╥
		character3 = 219;//█
		break;
	case PathCard::Type::BlindAlleyRightAndUp:
		character1 = 219;//█
		character2 = 208;//╨
		character3 = 198;//╞
		break;
	case PathCard::Type::BlindAlleyLeftAndUp:
		character1 = 181;//╡
		character2 = 208;//╨
		character3 = 219;//█
		break;
	case PathCard::Type::BlindAlleyRightAndDown:
		character1 = 219;//█
		character2 = 210;//╥
		character3 = 198;//╞
		break;
	case PathCard::Type::BlindAlleyLeftUpAndDown:
		character1 = 181;//█
		character2 = 215;//╫
		character3 = 219;//█
		break;
	case PathCard::Type::BlindAlleyRightUpAndDown:
		character1 = 219;//╡
		character2 = 215;//╫
		character3 = 198;//█
		break;
	case PathCard::Type::BlindAlleyLeftRightAndUp:
		character1 = 219;//╡
		character2 = 208;//╨
		character3 = 198;//╞
		break;
	case PathCard::Type::BlindAlleyLeftRightAndDown:
		character1 = 219;//╡
		character2 = 210;//╥
		character3 = 198;//╞
		break;
	}
	return os << character1 << character2 << character3;
}
