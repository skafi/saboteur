#include "ActionCard.h"

ActionCard::ActionCard() : ActionCard(Type::None)
{
	//EMPTY
}

ActionCard::ActionCard(const Type & type) : m_type(type)
{
	//EMPTY
}

ActionCard::ActionCard(const ActionCard & other)
{
	*this = other;
}

ActionCard::ActionCard(ActionCard && other)
{
	*this = std::move(other);
}

ActionCard & ActionCard::operator=(const ActionCard & other)
{
	m_type = other.m_type;

	return *this;
}

ActionCard & ActionCard::operator=(ActionCard && other)
{
	m_type = other.m_type;

	new(&other) ActionCard;

	return *this;
}

ActionCard::Type ActionCard::getType() const
{
	return m_type;
}

bool ActionCard::operator==(const ActionCard & other) const
{
	if (m_type == other.m_type)
		return true;
	return false;
}

std::ostream & operator<<(std::ostream & os, const ActionCard & actionCard)
{
	switch (static_cast<int>(actionCard.m_type))
	{
	case 1:
		os << "Break a Player Lamp";
		break;
	case 2:
		os << "Break a Player Cart";
		break;
	case 3:
		os << "Break a Player Pickaxe";
		break;
	case 4:
		os << "Fix a Player Lamp";
		break;
	case 5:
		os << "Fix a Player Cart";
		break;
	case 6:
		os << "Fix a Player Pickaxe";
		break;
	case 7:
		os << "Fix a Player Pickaxe Or Lamp";
		break;
	case 8:
		os << "Fix a Player Pickaxe Or Cart";
		break;
	case 9:
		os << "Fix a Player Lamp Or Cart";
		break;
	case 10:
		os << "View one of the destination cards";
		break;
	case 11:
		os << "Destroy a Path Card";
		break;
	default:
		os << "None";
		break;
	}
	return os;
}

