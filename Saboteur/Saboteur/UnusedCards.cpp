#include "UnusedCards.h"
#include <algorithm>
#include <random>      
#include <chrono>  


UnusedCards::UnusedCards(const UnusedCards & other)
{
	*this = other;
}

UnusedCards::UnusedCards(UnusedCards && other)
{
	*this = std::move(other);
}

UnusedCards & UnusedCards::operator=(const UnusedCards & other)
{
	m_cardPack = other.m_cardPack;

	return *this;
}

UnusedCards & UnusedCards::operator=(UnusedCards && other)
{
	m_cardPack = other.m_cardPack;

	new (&other)UnusedCards;
	return *this;
}

void UnusedCards::generateCardPack()
{
	//generate action cards
	m_cardPack.insert(m_cardPack.end(), 3, ActionCard(ActionCard::Type::BreakCart));
	m_cardPack.insert(m_cardPack.end() - 1, 3, ActionCard(ActionCard::Type::BreakLamp));
	m_cardPack.insert(m_cardPack.end() - 1, 3, ActionCard(ActionCard::Type::BreakPickaxe));
	m_cardPack.insert(m_cardPack.end() - 1, 2, ActionCard(ActionCard::Type::FixCart));
	m_cardPack.insert(m_cardPack.end() - 1, 2, ActionCard(ActionCard::Type::FixPickaxe));
	m_cardPack.insert(m_cardPack.end() - 1, 2, ActionCard(ActionCard::Type::FixPickaxe));
	m_cardPack.insert(m_cardPack.end() - 1, 1, ActionCard(ActionCard::Type::FixLampOrCart));
	m_cardPack.insert(m_cardPack.end() - 1, 1, ActionCard(ActionCard::Type::FixPickaxeOrCart));
	m_cardPack.insert(m_cardPack.end() - 1, 1, ActionCard(ActionCard::Type::FixPickaxeOrLamp));
	m_cardPack.insert(m_cardPack.end() - 1, 3, ActionCard(ActionCard::Type::DestroyPath));
	m_cardPack.insert(m_cardPack.end() - 1, 6, ActionCard(ActionCard::Type::View));

	//generate path cards
	m_cardPack.insert(m_cardPack.end() - 1, 5, PathCard(PathCard::Type::Junction));
	m_cardPack.insert(m_cardPack.end() - 1, 3, PathCard(PathCard::Type::UpAndDown));
	m_cardPack.insert(m_cardPack.end() - 1, 4, PathCard(PathCard::Type::LeftAndRight));
	m_cardPack.insert(m_cardPack.end() - 1, 5, PathCard(PathCard::Type::UpAndLeft));
	m_cardPack.insert(m_cardPack.end() - 1, 4, PathCard(PathCard::Type::UpAndRight));
	m_cardPack.insert(m_cardPack.end() - 1, 5, PathCard(PathCard::Type::UpDownAndLeft));
	m_cardPack.insert(m_cardPack.end() - 1, 5, PathCard(PathCard::Type::LeftRightAndDown));
	m_cardPack.insert(m_cardPack.end() - 1, 1, PathCard(PathCard::Type::BlindAlleyLeft));
	m_cardPack.insert(m_cardPack.end() - 1, 1, PathCard(PathCard::Type::BlindAlleyDown));
	m_cardPack.insert(m_cardPack.end() - 1, 1, PathCard(PathCard::Type::BlindAlleyJunction));
	m_cardPack.insert(m_cardPack.end() - 1, 1, PathCard(PathCard::Type::BlindAlleyUpAndDown));
	m_cardPack.insert(m_cardPack.end() - 1, 1, PathCard(PathCard::Type::BlindAlleyLeftAndRight));
	m_cardPack.insert(m_cardPack.end() - 1, 1, PathCard(PathCard::Type::BlindAlleyLeftAndDown));
	m_cardPack.insert(m_cardPack.end() - 1, 1, PathCard(PathCard::Type::BlindAlleyLeftAndUp));
	m_cardPack.insert(m_cardPack.end() - 1, 1, PathCard(PathCard::Type::BlindAlleyLeftUpAndDown));
	m_cardPack.insert(m_cardPack.end() - 1, 1, PathCard(PathCard::Type::BlindAlleyLeftRightAndDown));

	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::shuffle(m_cardPack.begin(), m_cardPack.end(), std::default_random_engine(seed));

}
