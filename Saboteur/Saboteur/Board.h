#pragma once
#include "PathCard.h"
#include "BeginCard.h"

#include <iostream>
#include <array>
#include <optional>
#include<variant>
class Board
{
public:
	using Position = std::pair<unsigned short, unsigned short>;

	Board() = default;
	~Board() = default;

	// Getter
	const std::optional<std::variant<PathCard, BeginCard>>& operator[] (const Position& pos) const;
	// Getter and/or Setter
	std::optional<std::variant<PathCard, BeginCard>>& operator[] (const Position& pos);

	friend std::ostream& operator << (std::ostream& os, const Board& board);

	static const short getBoardWidth();
	static const short getBoardHeight();

private:
	static const short kWidth = 5;
	static const short kHeight = 9;
	static const short kSize = kWidth * kHeight;

private:
	std::array<std::optional<std::variant<PathCard, BeginCard>>, kSize> m_cards;
};

