#include "Player.h"

std::string Player::getName() const
{
	return m_name;
}

void Player::setName(const std::string & name)
{
	m_name = name;
}

short Player::getAge() const
{
	return m_age;
}

void Player::setAge(const short &age)
{
	m_age = age;
}

DwarfCard Player::getRole() const
{
	return m_role;
}

void Player::setRole(const DwarfCard &role)
{
	m_role = role;
}

short Player::getScore() const
{
	return m_score;
}

void Player::setScore(const short &score)
{
	m_score = score;
}

bool Player::getStatus() const
{
	return m_blocked;
}

void Player::setStatus(const bool &status)
{
	m_blocked = status;
}

std::vector<ActionCard> Player::getCardsThatBlockMe() const
{
	return m_blockedCards;
}

void Player::setCardsThatBlockMe(const std::vector<ActionCard>&blockCards)
{
	m_blockedCards = blockCards;
}

std::vector<std::variant<PathCard, ActionCard>> Player::getHandPackage() const
{
	return m_handPackage;
}

void Player::setHandPackage(std::vector<std::variant<PathCard, ActionCard>>handPackage)
{
	m_handPackage = handPackage;
}

bool Player::operator < (const Player & other) const
{
	return this->m_age < other.m_age;
}

bool Player::operator==(const Player & other) const
{
	if (m_name.compare(other.m_name) == 0)
		return true;
	return false;
}

std::variant<PathCard, ActionCard> Player::chooseACard() const
{
	std::cout << "\nTO PICK A CARD";
	std::cout << "\nChoose a number between 1 and " << m_handPackage.size() << ":\n";
	int cardNumber;
	do
	{
		std::cin >> cardNumber;
		while (std::cin.fail()) {
			std::cout << "Enter a number between 1 and " << m_handPackage.size() << ":\n";
			if (std::cin.fail())
			{
				std::cin.clear();
				std::cin.ignore(256, '\n');
			}
			std::cin >> cardNumber;
		}
		if (cardNumber > m_handPackage.size() || cardNumber < 1)
			std::cout << "\nWRONG NUMBER\n";
	} while (cardNumber < 1 || cardNumber > m_handPackage.size());
	return m_handPackage[cardNumber - 1];
}

void Player::drawCard(std::vector<std::variant<PathCard, ActionCard>>&cardPack)
{
	if (!cardPack.empty())
	{
		std::variant<PathCard, ActionCard> drewCard = cardPack.back();
		this->m_handPackage.push_back(drewCard);
		cardPack.pop_back();
	}
	else
		std::cout << "There are no cards left";
}

void Player::discard(std::variant<PathCard, ActionCard>&toDiscard, std::vector<std::variant<PathCard, ActionCard>>&cardPack)
{
	m_handPackage.erase(std::find(m_handPackage.begin(), m_handPackage.end(), toDiscard));
	this->drawCard(cardPack);
}

Board::Position Player::placePathCard(const PathCard & pathCard, const short line, const short column, Board & board) const
{
	Board::Position position = { line, column };
	if (board[position].has_value())
		throw "That position is already ocupied";
	else
		board[position] = pathCard;
	return position;
}

bool Player::blockPlayer(Player & playerToBlock, const ActionCard card) const
{
	std::vector<ActionCard> blockCards = playerToBlock.getCardsThatBlockMe();
	if (std::find(blockCards.begin(), blockCards.end(), card) == blockCards.end())
	{
		blockCards.push_back(card);
		playerToBlock.setCardsThatBlockMe(blockCards);
		playerToBlock.setStatus(true);
		return true;
	}
	return false;
}

bool Player::unblockPlayer(Player & playerToUnblock, const ActionCard card) const
{
	std::vector<ActionCard> blockCards = playerToUnblock.getCardsThatBlockMe();
	std::vector<ActionCard>::iterator cardToFind;
	bool ok = false;
	for (cardToFind = blockCards.begin(); cardToFind < blockCards.end(); ++cardToFind)
	{
		if (cardToFind->getType() == ActionCard::Type::BreakCart)
			if (card.getType() == ActionCard::Type::FixCart || card.getType() == ActionCard::Type::FixLampOrCart
				|| card.getType() == ActionCard::Type::FixPickaxeOrCart)
			{
				ok = true;
				break;
			}
		if (cardToFind->getType() == ActionCard::Type::BreakLamp)
			if (card.getType() == ActionCard::Type::FixLamp || card.getType() == ActionCard::Type::FixLampOrCart
				|| card.getType() == ActionCard::Type::FixPickaxeOrLamp)
			{
				ok = true;
				break;
			}
		if (cardToFind->getType() == ActionCard::Type::BreakPickaxe)
			if (card.getType() == ActionCard::Type::FixPickaxe || card.getType() == ActionCard::Type::FixPickaxeOrCart
				|| card.getType() == ActionCard::Type::FixPickaxeOrLamp)
			{
				ok = true;
				break;
			}
	}
	if (ok)
	{
		blockCards.erase(cardToFind);
		playerToUnblock.setCardsThatBlockMe(blockCards);
		if (blockCards.size() == 0)
			playerToUnblock.setStatus(false);
		return true;
	}
	return false;
}

bool Player::destroyPathCard(Board & board, Board::Position & position) const
{
	if (board[position].has_value())
		if (std::holds_alternative<PathCard>(board[position].value()))
		{
			board[position] = std::nullopt;
			return true;
		}
	return false;
}

std::string Player::viewDestinationCard(Board & board) const
{
	short cardNumber;
	do
	{
		std::cout << "Which card do you want to check? 1, 2 or 3?";
		std::cin >> cardNumber;
		if (cardNumber < 0 || cardNumber > 3)
			std::cout << "Choose a number between 1 and 3\n";
	} while (cardNumber < 0 || cardNumber > 3);
	//all the destination cards will be on line 0
	short line = 0;
	/* there are 3 destination cards on columns: 0, 2, 4;
		this is the formula which calculates the standard position
		of each card s column;
	*/
	short column = cardNumber * 2 - 2;

	Board::Position position = { line, column };
	if (std::holds_alternative<BeginCard>(*board[position]))
	{
		BeginCard cardToView = std::get<BeginCard>(*board[position]);
		if (cardToView.getType() == BeginCard::Type::Gold)
			return "Gold";
		else
			return "Rock";
	}
}
