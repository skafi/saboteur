#pragma once
#include<iostream>
class PathCard
{
public:
	enum class Type : uint8_t
	{
		None,
		Junction,
		UpAndDown,
		LeftAndRight,
		UpAndLeft,
		DownAndRight,
		UpAndRight,
		DownAndLeft,
		UpDownAndLeft,
		UpDownAndRight,
		LeftRightAndDown,
		LeftRightAndUp,
		BlindAlleyLeft,
		BlindAlleyRight,
		BlindAlleyUp,
		BlindAlleyDown,
		BlindAlleyJunction,
		BlindAlleyUpAndDown,
		BlindAlleyLeftAndRight,
		BlindAlleyLeftAndDown,
		BlindAlleyRightAndUp,
		BlindAlleyLeftAndUp,
		BlindAlleyRightAndDown,
		BlindAlleyLeftUpAndDown,
		BlindAlleyRightUpAndDown,
		BlindAlleyLeftRightAndUp,
		BlindAlleyLeftRightAndDown
	};

	PathCard();
	PathCard(const Type& type);
	PathCard(const PathCard &other);
	PathCard(PathCard &&other);
	~PathCard() = default;

	Type getType() const;
	bool getNorth()const;
	bool getSouth()const;
	bool getEast()const;
	bool getWest()const;
	bool getCenter()const;

	PathCard & operator = (const PathCard &other);
	PathCard & operator = (PathCard &&other);
	bool operator==(const PathCard& other) const;

	friend std::ostream& operator << (std::ostream& os, const PathCard& pathCard);

	PathCard rotateCard(const PathCard& card) const;

private:
	Type m_type : 5;
	bool m_north = false;
	bool m_south = false;
	bool m_east = false;
	bool m_west = false;
	bool m_center = false;
};

