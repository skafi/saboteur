#pragma once
#include<iostream>
class GoldCard
{
public:
	enum class Type : uint8_t
	{
		None,
		OnePieceGold,
		TwoPiecesGold,
		ThreePiecesGold

	};

	Type getType() const;
	GoldCard() = default;
	GoldCard(const Type& type);
	GoldCard(const GoldCard &other);
	GoldCard(GoldCard &&other);
	~GoldCard() = default;

	GoldCard & operator = (const GoldCard &other);
	GoldCard & operator = (GoldCard &&other);
	bool operator < (const GoldCard& other)const;

	friend std::ostream& operator << (std::ostream& os, const GoldCard& goldCard);

private:
	Type m_type : 3;
};

