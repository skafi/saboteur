#include"Game.h"
#include "../Logging/Logging.h"
#include <fstream>
int main()
{
	Game game;

	std::ofstream of("log.log", std::ios::app);
	Logger log(of);
	log.log("Started the game...", Logger::Level::Info);

	game.startGame();
	system("pause");
}