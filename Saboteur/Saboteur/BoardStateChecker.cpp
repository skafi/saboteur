#include "BoardStateChecker.h"

BoardStateChecker::State BoardStateChecker::Check(Board& board)
{
	short index;
	for (index = 0; index < board.getBoardWidth(); index += 2)
		if (std::holds_alternative<BeginCard>(board[{0, index}].value()))
		{
			BeginCard destinationCard = std::get<BeginCard>(board[{0, index}].value());
			if (destinationCard.getType() == BeginCard::Type::Gold)
			{
				std::optional<PathCard> down, left, right;
				if (board[{1, index}].has_value())
					down = std::get<PathCard>(board[{1, index}].value());
				if (index > 0 && board[{0, index - 1}].has_value())
					left = std::get<PathCard>(board[{0, index - 1}].value());
				if (index < board.getBoardWidth() - 1 && board[{0, index + 1}].has_value())
					right = std::get<PathCard>(board[{0, index + 1}].value());
				if (!(down.has_value() || left.has_value() || right.has_value()))
					return State::None;
				break;
			}
		}

	//if (isPath(Board::Position({ board.getBoardHeight() - 1, board.getBoardWidth() / 2 }), Board::Position({ 0, index }), board))
		//return State::GoldDiggerWin;

	if (aStarAlgorithm(Board::Position({ board.getBoardHeight() - 1, board.getBoardWidth() / 2 }), Board::Position({ 0, index }), board))
		return State::GoldDiggerWin;
	return State::None;
}

bool BoardStateChecker::isPath(const Board::Position &positionBegin, const Board::Position &positionEnd, Board & board)
{
	std::vector<Board::Position> toVisit;
	std::vector<Board::Position> visited;
	toVisit.emplace_back(positionBegin);
	while (toVisit.size() > 0)
	{
		Board::Position position = toVisit.at(0);
		if (position == positionEnd)
			return true;
		auto line = position.first;
		auto column = position.second;
		board[{board.getBoardHeight() - 1, board.getBoardWidth() / 2}] = PathCard(PathCard::Type::Junction);
		if (std::holds_alternative<PathCard>(board[position].value()) &&
			std::get<PathCard>(board[position].value()).getCenter())
		{
			if (line < board.getBoardHeight() - 1)
				if (board[{line + 1, column}].has_value())
					addElement(toVisit, visited, { line + 1, column });
			if (line > 0)
				if (board[{line - 1, column}].has_value())
					addElement(toVisit, visited, { line - 1,column });
			if (column < board.getBoardWidth() - 1)
				if (board[{line, column + 1}].has_value())
					addElement(toVisit, visited, { line, column + 1 });
			if (column > 0)
				if (board[{line, column - 1}].has_value())
					addElement(toVisit, visited, { line, column - 1 });
		}
		visited.push_back(toVisit.at(0));
		toVisit.erase(toVisit.begin());
	}
	board[{board.getBoardHeight() - 1, board.getBoardWidth() / 2}] = BeginCard(BeginCard::Type::Ladder);

	return false;
}

bool BoardStateChecker::aStarAlgorithm(const Board::Position & positionBegin, const Board::Position & positionEnd, Board & board)
{
	Node* node = new Node();

	//position coordonates on board
	unsigned short x = positionBegin.first;
	unsigned short y = positionBegin.second;
	const unsigned short destinationX = positionEnd.first;
	const unsigned short destinationY = positionEnd.second;
	node->position = { x, y };

	node->heuristicCost = (unsigned short)sqrt(pow(destinationX - x, 2) + pow(destinationY - y, 2));
	node->totalCost = 0 + node->heuristicCost;

	std::multimap<unsigned short, Node*> toVisit;

	toVisit.insert(std::make_pair(node->totalCost, node));

	std::vector<Board::Position> visited;

	while (!toVisit.empty())
	{
		node = toVisit.begin()->second;

		board[{board.getBoardHeight() - 1, board.getBoardWidth() / 2}] = PathCard(PathCard::Type::Junction);

		Board::Position position = node->position;

		if (position == positionEnd)
			return true;
		auto line = position.first;
		auto column = position.second;

		if (std::holds_alternative<PathCard>(board[position].value()) &&
			std::get<PathCard>(board[position].value()).getCenter())
		{
			if (line < board.getBoardHeight() - 1)
				if (board[{line + 1, column}].has_value())
				{
					Board::Position position = { line + 1, column };
					add(toVisit, visited, board, position, node, destinationX, destinationY);
				}
			if (line > 0)
				if (board[{line - 1, column}].has_value())
				{
					Board::Position position = { line - 1, column };
					add(toVisit, visited, board, position, node, destinationX, destinationY);
				}
			if (column < board.getBoardWidth() - 1)
				if (board[{line, column + 1}].has_value())
				{
					Board::Position position = { line, column + 1 };
					add(toVisit, visited, board, position, node, destinationX, destinationY);
				}
			if (column > 0)
				if (board[{line, column - 1}].has_value())
				{
					Board::Position position = { line, column - 1 };
					add(toVisit, visited, board, position, node, destinationX, destinationY);
				}
		}
		visited.push_back(toVisit.begin()->second->position);
		toVisit.erase(toVisit.begin());
	}
	board[{board.getBoardHeight() - 1, board.getBoardWidth() / 2}] = BeginCard(BeginCard::Type::Ladder);

	return false;
}

void BoardStateChecker::addElement(std::vector<Board::Position>&toVisit, const std::vector<Board::Position>&visited,
	const Board::Position position)
{
	if (std::find(visited.begin(), visited.end(), position) == visited.end())
		toVisit.emplace_back(position);
}

void BoardStateChecker::add(std::multimap<unsigned short, Node*>&toVisit, std::vector<Board::Position>&visited,
	const Board board, const Board::Position position, Node* node,
	const unsigned short destinationX, const unsigned short destinationY)
{
	unsigned short line = position.first;
	unsigned short column = position.second;
	if (board[position].has_value())
		if (std::find(visited.begin(), visited.end(), position) == visited.end())
		{
			Node* neighbour = new Node();
			neighbour->heuristicCost = (unsigned short)sqrt(pow(destinationX - (line + 1), 2) + pow(destinationY - column, 2));
			unsigned short temporaryCost = neighbour->heuristicCost + node->totalCost + node->movementCost;

			neighbour->parent = node;
			neighbour->position = position;
			neighbour->heuristicCost = (unsigned short)sqrt(pow(destinationX - position.first, 2) + pow(destinationY - position.second, 2));
			neighbour->totalCost = temporaryCost;

			toVisit.insert(std::make_pair(neighbour->totalCost, neighbour));
		}
}