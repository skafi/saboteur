#pragma once
#include "Board.h"

#include<vector>
#include <memory>
#include <utility>
#include <math.h>
#include <map>
class BoardStateChecker
{
public:
	enum class State
	{
		None,
		GoldDiggerWin
	};
public:
	static State Check(Board& board);
	static bool isPath(const Board::Position&, const Board::Position&, Board&);
	static bool aStarAlgorithm(const Board::Position &positionBegin, const Board::Position &positionEnd, Board & board);
private:
	struct Node
	{
		Node* parent;
		Board::Position position;
		const unsigned short movementCost = 1;
		unsigned short heuristicCost;
		unsigned short totalCost;

		Node::Node() = default;

		Node::Node(const Node & other)
		{
			*this = other;
		}

		Node & operator = (const Node &other)
		{
			heuristicCost = other.heuristicCost;
			totalCost = other.totalCost;
			parent = other.parent;

			return *this;
		}

		Node::Node(Node && other)
		{
			*this = std::move(other);
		}

		Node & Node::operator=(Node && other)
		{
			heuristicCost = other.heuristicCost;
			totalCost = other.totalCost;
			parent = other.parent;

			new(&other) Node;

			return *this;
		}

		bool operator< (const Node& other) const
		{
			if (other.totalCost < this->totalCost)
				return true;
		}
	};
	static void addElement(std::vector<Board::Position>&, const std::vector<Board::Position>&, const Board::Position);
	static void add(std::multimap<unsigned short, Node*>&, std::vector<Board::Position>&, const Board,
		const Board::Position, Node*, const unsigned short, const unsigned short);
};

