﻿#include "BeginCard.h"

BeginCard::BeginCard(const Type& type) : m_type(type)
{
	//EMPTY
}

BeginCard::BeginCard(const BeginCard & other)
{
	*this = other;
}

BeginCard::BeginCard(BeginCard && other)
{
	*this = std::move(other);
}

BeginCard & BeginCard::operator=(const BeginCard & other)
{
	m_type = other.m_type;
	return *this;
}

BeginCard & BeginCard::operator=(BeginCard && other)
{
	m_type = other.m_type;

	new(&other) BeginCard;

	return *this;
}

BeginCard::Type BeginCard::getType() const
{
	return m_type;
}

std::ostream & operator<<(std::ostream & os, const BeginCard & card)
{
	std::string displayCard;
	char character1 = '?', character2 = '?', character3 = '?';
	if (card.m_type == BeginCard::Type::Ladder)
	{
		character1 = 205;//═
		character2 = 206;//╬
		character3 = 205;//═
	}
	os << character1 << character2 << character3;
	return os;
}
