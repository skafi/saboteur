#pragma once

#include "Board.h"
#include "ActionCard.h"
#include "DwarfCard.h"

#include<iostream>
#include<string>
#include<vector>

class Player
{
public:
	Player() = default;
	~Player() = default;

	std::string getName()const;
	void setName(const std::string&);
	short getAge()const;
	void setAge(const short&);
	DwarfCard getRole() const;
	void setRole(const DwarfCard&);
	short getScore()const;
	void setScore(const short&);
	bool getStatus()const; //blocked/unblocked
	void setStatus(const bool&);
	std::vector<ActionCard> getCardsThatBlockMe()const;
	void setCardsThatBlockMe(const std::vector<ActionCard>&);
	std::vector<std::variant<PathCard, ActionCard>> getHandPackage()const;
	void setHandPackage(std::vector<std::variant<PathCard, ActionCard>>);

	bool operator < (const Player& other) const;
	bool operator==(const Player& other) const;

	std::variant<PathCard, ActionCard> chooseACard()const;
	void drawCard(std::vector<std::variant<PathCard, ActionCard>>&);
	void discard(std::variant<PathCard, ActionCard>&, std::vector<std::variant<PathCard, ActionCard>>&);
	Board::Position placePathCard(const PathCard& pathCard, const short line, const short column, Board& board)const;
	bool blockPlayer(Player& playerToBlock, const ActionCard card)const;
	bool unblockPlayer(Player& playerToUnblock, const ActionCard card)const;
	bool destroyPathCard(Board & board, Board::Position& position)const;
	std::string viewDestinationCard(Board& board) const;

private:
	std::string m_name;
	short m_age : 7;
	DwarfCard m_role;
	short m_score = 0;
	bool m_blocked = false;
	std::vector<ActionCard> m_blockedCards;
	std::vector<std::variant<PathCard, ActionCard>> m_handPackage;
};

