#include "Display.h"
#include <fstream>

void Display::displayBoard(const Board & board)
{
	std::cout << board;
}

void Display::displayPlayerCards(Player player)
{
	std::cout << "\nYOUR CARDS ARE:\n";
	int index = 1;
	for (std::variant<PathCard, ActionCard> card : player.getHandPackage())
	{
		if (std::holds_alternative<PathCard>(card))
		{
			const PathCard pathCard = std::get<PathCard>(card);
			std::cout << "\n\t" << index << ".PathCard: " << pathCard << "\n";
		}
		else
			if (std::holds_alternative<ActionCard>(card))
			{
				const ActionCard actionCard = std::get<ActionCard>(card);
				std::cout << "\n\t" << index << ".ActionCard: " << actionCard << "\n";
			}
		++index;
	}
}

void Display::displayPlayerNameAndStatus(Player player)
{
	std::cout << "PLAYER: " << player.getName() << "\nSTATUS: ";
	if (player.getStatus())
	{
		std::cout << "BLOCKED\nCards that blocks you:";
		for (ActionCard blockCard : player.getCardsThatBlockMe())
			std::cout << blockCard << "\n";
	}
	else
		std::cout << "UNBLOCKED\n";
	if (player.getRole().getType() == DwarfCard::Type::Saboteur)
		std::cout << "SABOTEUR\n";
	else
		std::cout << "GOLD DIGGER\n";
	std::cout << "GOLD: " << player.getScore() << "\n";
}

void Display::displayGameName()
{
	std::cout << "\t\t ___     __     ____   _____   ____   ____   __  __   ____  \n";
	std::cout << "\t\t/ __)   /__\\\   (  _ \\\ (  _  ) (_  _) ( ___) (  )(  ) (  _ \\\ \n";
	std::cout << "\t\t\\\__ \\\  /(__)\\\   ) _ <  )(_)(    )(    )__)   )(__)(   )   / \n";
	std::cout << "\t\t(___/ (__)(__) (____/ (_____)  (__)  (____) (______) (_)\\\_) \n\n";
	std::cout << "\t\tMADE BY: MIHAIU IULIA, SCAFARIU BOGDAN AND SALAGEAN BOGDAN\n";
}

void Display::displayImage(const std::string& fileName)
{

	std::ifstream file;
	file.open(fileName);
	std::string output;
	if (file.is_open())
	{
		while (!file.eof())
		{
			std::getline(file, output, '\0');
			std::cout << output << "\n";

		}
	}
	file.close();
}
