#include "DwarfCard.h"

DwarfCard::DwarfCard() : DwarfCard(Type::None)
{
	//EMPTY
}

DwarfCard::DwarfCard(const Type& type) : m_type(type)
{
	//EMPTY
}

DwarfCard::DwarfCard(const DwarfCard & other)
{
	*this = other;
}

DwarfCard::DwarfCard(DwarfCard && other)
{
	*this = std::move(other);
}

DwarfCard & DwarfCard::operator=(const DwarfCard & other)
{
	m_type = other.m_type;
	return *this;
}

DwarfCard & DwarfCard::operator=(DwarfCard && other)
{
	m_type = other.m_type;

	new(&other) DwarfCard;

	return *this;
}

DwarfCard::Type DwarfCard::getType() const
{
	return m_type;
}

std::ostream & operator<<(std::ostream & os, const DwarfCard & dwarfCard)
{
	return os << static_cast<int>(dwarfCard.m_type);
}