#include "Game.h"
#include "Display.h"
#include "../Logging/Logging.h"
#include <fstream>

void Game::startGame()
{
	Display::displayImage("SaboteurImage.txt");
	Display::displayGameName();
	system("pause");
	system("CLS");
	generatePlayers();
	generateGoldCards();
	for (int index = 0; index < 3; index++)
	{
		system("CLS");
		std::cout << "\tROUND " << index + 1 << "/3\n";
		system("pause");
		generateDwarfCards();
		distributePlayerRoles();
		generateCardPack();
		distributeCards();
		generateBeginCards();
		initializeBoard();
		bool gameLoop = false;
		while (!gameLoop)
		{
			std::vector<Player>::iterator iterator = m_listOfPlayers.begin();
			for (iterator; iterator != m_listOfPlayers.end(); ++iterator)
			{
				system("CLS");
				Display::displayBoard(m_board);
				Display::displayPlayerNameAndStatus(*iterator);
				Display::displayPlayerCards(*iterator);
				executeAction(*iterator, iterator->chooseACard());
				system("pause");
				gameLoop = endOfTurn();
				if (gameLoop)
				{
					system("CLS");
					if(BoardStateChecker::Check(m_board) == BoardStateChecker::State::GoldDiggerWin)
						distributeGold(true, *iterator);
					else
						distributeGold(false, *iterator);
					std::cout << m_board;
					reinitializePlayers();
					system("pause");
					break;
				}
			}
		}
	}
	system("CLS");
	for (auto iterator : m_listOfPlayers)
		std::cout << iterator.getName() << " : " << iterator.getScore() << "\n";
	std::cout << "\n\n";
	Display::displayImage("HappyDwarf.txt");
}

void Game::generatePlayers()
{

	std::ofstream of("log.log", std::ios::app);
	Logger log(of);
	log.log("Generated players...", Logger::Level::Info);

	std::cout << "Number of players (between 3 and 10): \n";
	//minimum 3 players, maximum 10
	do
	{
		std::cin >> m_numberOfPlayers;
		if (m_numberOfPlayers > 10 || m_numberOfPlayers < 3)
			std::cout << "Incorrect Number, write a number between 3 and 10\n";
	} while (m_numberOfPlayers < 3 || m_numberOfPlayers > 10);
	m_listOfPlayers.insert(m_listOfPlayers.begin(), m_numberOfPlayers, Player());
	short playerNumber = 1;
	for (auto iterator = m_listOfPlayers.begin(); iterator != m_listOfPlayers.end(); ++iterator)
	{
		std::string playerName;
		short playerAge;
		do
		{
			std::cout << "Player" << playerNumber << " name:";
			std::cin >> playerName;
			if (searchPlayerByName(playerName)!=m_listOfPlayers.end())
				std::cout << "\nTHIS PLAYER ALREADY EXISTS\n";
		} while (searchPlayerByName(playerName) != m_listOfPlayers.end());
		iterator->setName(playerName);
		std::cout << "Player" << playerNumber << " age:";
		std::cin >> playerAge;
		while (std::cin.fail()) {
			std::cout << "Player" << playerNumber << " age:";
			if (std::cin.fail())
			{
				std::cin.clear();
				std::cin.ignore(256, '\n');
			}
			std::cin >> playerAge;
		}
			iterator->setAge(playerAge);
			++playerNumber;
	}
	std::sort(m_listOfPlayers.begin(), m_listOfPlayers.end());
}

void Game::generateDwarfCards()
{
	std::ofstream of("log.log", std::ios::app);
	Logger log(of);
	log.log("Generated dwarf cards...", Logger::Level::Info);

	m_listOfDwarfCards = std::vector<DwarfCard>();
	switch (m_numberOfPlayers)
	{
	case 3:
		m_listOfDwarfCards.insert(m_listOfDwarfCards.end(), 3, DwarfCard(DwarfCard::Type::GoldDigger));
		m_listOfDwarfCards.insert(m_listOfDwarfCards.end(), 1, DwarfCard(DwarfCard::Type::Saboteur));
		break;
	case 4:
		m_listOfDwarfCards.insert(m_listOfDwarfCards.end(), 4, DwarfCard(DwarfCard::Type::GoldDigger));
		m_listOfDwarfCards.insert(m_listOfDwarfCards.end(), 1, DwarfCard(DwarfCard::Type::Saboteur));
		break;
	case 5:
		m_listOfDwarfCards.insert(m_listOfDwarfCards.end(), 4, DwarfCard(DwarfCard::Type::GoldDigger));
		m_listOfDwarfCards.insert(m_listOfDwarfCards.end(), 2, DwarfCard(DwarfCard::Type::Saboteur));
		break;
	case 6:
		m_listOfDwarfCards.insert(m_listOfDwarfCards.end(), 5, DwarfCard(DwarfCard::Type::GoldDigger));
		m_listOfDwarfCards.insert(m_listOfDwarfCards.end(), 2, DwarfCard(DwarfCard::Type::Saboteur));
		break;
	case 7:
		m_listOfDwarfCards.insert(m_listOfDwarfCards.end(), 5, DwarfCard(DwarfCard::Type::GoldDigger));
		m_listOfDwarfCards.insert(m_listOfDwarfCards.end(), 3, DwarfCard(DwarfCard::Type::Saboteur));
		break;
	case 8:
		m_listOfDwarfCards.insert(m_listOfDwarfCards.end(), 6, DwarfCard(DwarfCard::Type::GoldDigger));
		m_listOfDwarfCards.insert(m_listOfDwarfCards.end(), 3, DwarfCard(DwarfCard::Type::Saboteur));
		break;
	case 9:
		m_listOfDwarfCards.insert(m_listOfDwarfCards.end(), 7, DwarfCard(DwarfCard::Type::GoldDigger));
		m_listOfDwarfCards.insert(m_listOfDwarfCards.end(), 3, DwarfCard(DwarfCard::Type::Saboteur));
		break;
	case 10:
		m_listOfDwarfCards.insert(m_listOfDwarfCards.end(), 7, DwarfCard(DwarfCard::Type::GoldDigger));
		m_listOfDwarfCards.insert(m_listOfDwarfCards.end(), 4, DwarfCard(DwarfCard::Type::Saboteur));
		break;
	}
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::shuffle(m_listOfDwarfCards.begin(), m_listOfDwarfCards.end(), std::default_random_engine(seed));
}

void Game::generateBeginCards()
{
	std::ofstream of("log.log", std::ios::app);
	Logger log(of);
	log.log("Generated begin cards...", Logger::Level::Info);

	m_listOfBeginCards = std::vector<BeginCard>();
	m_listOfBeginCards.insert(m_listOfBeginCards.begin(), 2, BeginCard::Type::Rock);
	m_listOfBeginCards.insert(m_listOfBeginCards.end(), 1, BeginCard::Type::Gold);
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::shuffle(m_listOfBeginCards.begin(), m_listOfBeginCards.end(), std::default_random_engine(seed));
	m_listOfBeginCards.insert(m_listOfBeginCards.end(), 1, BeginCard::Type::Ladder);
}

void Game::generateCardPack()
{
	std::ofstream of("log.log", std::ios::app);
	Logger log(of);
	log.log("Generated card pack...", Logger::Level::Info);

	m_unusedCards = UnusedCards();
	m_unusedCards.generateCardPack();
}

void Game::generateGoldCards()
{
	std::ofstream of("log.log", std::ios::app);
	Logger log(of);
	log.log("Generated gold cards...", Logger::Level::Info);

	m_listOfGoldCards.insert(m_listOfGoldCards.end(), 16, GoldCard(GoldCard::Type::OnePieceGold));
	m_listOfGoldCards.insert(m_listOfGoldCards.end(), 8, GoldCard(GoldCard::Type::TwoPiecesGold));
	m_listOfGoldCards.insert(m_listOfGoldCards.end(), 4, GoldCard(GoldCard::Type::ThreePiecesGold));
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::shuffle(m_listOfGoldCards.begin(), m_listOfGoldCards.end(), std::default_random_engine(seed));
}

void Game::distributePlayerRoles()
{
	std::ofstream of("log.log", std::ios::app);
	Logger log(of);
	log.log("Distributed the players roles...", Logger::Level::Info);

	short dwarfCardNumber = 0;
	for (auto iterator = m_listOfPlayers.begin(); iterator != m_listOfPlayers.end(); ++iterator)
	{
		iterator->setRole(m_listOfDwarfCards[dwarfCardNumber]);
		++dwarfCardNumber;
	}
}

void Game::distributeCards()
{
	std::ofstream of("log.log", std::ios::app);
	Logger log(of);
	log.log("Distributed cards...", Logger::Level::Info);

	int numberOfPlayerCards;
	if (m_numberOfPlayers < 6)
		numberOfPlayerCards = 6;
	else
		if (m_numberOfPlayers < 8)
			numberOfPlayerCards = 5;
		else
			numberOfPlayerCards = 4;

	for (auto iterator = m_listOfPlayers.begin(); iterator != m_listOfPlayers.end(); ++iterator)
		for (int index = 0; index < numberOfPlayerCards; index++)
			iterator->drawCard(m_unusedCards.m_cardPack);
}

void Game::initializeBoard()
{
	std::ofstream of("log.log", std::ios::app);
	Logger log(of);
	log.log("Generated board...", Logger::Level::Info);

	m_board = Board();
	for (int index = 0; index < 3; index++)
	{
		short line = 0;
		short column = index * 2;
		Board::Position position = { line, column };
		m_board[position] = m_listOfBeginCards[index];
	}
	m_board[{8, 2}] = m_listOfBeginCards[3];
}

void Game::executeAction(Player& player, std::variant<PathCard, ActionCard>choosedCard)
{
	if (std::holds_alternative<PathCard>(choosedCard))
	{
		std::ofstream of("log.log", std::ios::app);
		Logger log(of);
		log.log("Chose path card...", Logger::Level::Info);

		const PathCard card = std::get<PathCard>(choosedCard);
		std::cout << " You chose: " << card << "\n";
		std::cout << "\tCHOOSE AN OPTION\n\t1.PLACE\n\t2.DISCARD\n\t3.CHOOSE ANOTHER CARD\n";
		short optionNumber;
		do
		{
			optionNumber = chooseOption();
			if (player.getStatus() && optionNumber == 1)
				std::cout << "\n\tYOU ARE BLOCKED, YOU CAN'T PLACE CARDS\n";
		} while (player.getStatus() && optionNumber == 1);
		executePathCardAction(optionNumber, player, choosedCard);
	}
	else
	{
		std::ofstream of("log.log", std::ios::app);
		Logger log(of);
		log.log("Chose action card...", Logger::Level::Info);

		const ActionCard card = std::get<ActionCard>(choosedCard);
		std::cout << " You chose: " << card << "\n";
		std::cout << "\tWhat you want to do?\n\t1.USE CARD\n\t2.DISCARD\n\t3.CHOOSE ANOTHER CARD\n";
		short optionNumber = chooseOption();
		executeActionCardOption(optionNumber, player, choosedCard);
	}
}

bool Game::proceedAction()const
{
	bool proceed;
	std::cout << "Proceed or go back and choose another card? (0->GO BACK; 1->PROCEED)";
	std::cin >> proceed;
	while (std::cin.fail()) {
		std::cout << "Proceed or go back and choose another card? (0->GO BACK; 1->PROCEED)";
		if (std::cin.fail())
		{
			std::cin.clear();
			std::cin.ignore(256, '\n');
		}
		std::cin >> proceed;
	}
	return proceed;
}

short Game::chooseOption() const
{
	std::ofstream of("log.log", std::ios::app);
	Logger log(of);
	log.log("Chose option...", Logger::Level::Info);

	short optionNumber;
	do
	{
		std::cout << "Choose an option:";
		std::cin >> optionNumber;
		while (std::cin.fail()) {
			std::cout << "PLEASE ENTER A NUMBER!\n";
			std::cout << "Choose an option:";
			if (std::cin.fail())
			{
				std::cin.clear();
				std::cin.ignore(256, '\n');
			}
			std::cin >> optionNumber;
		}
		if (optionNumber < 1 || optionNumber > 3)
			std::cout << "Enter a number between 1 and 3\n";
	} while (optionNumber < 1 || optionNumber > 3);
	return optionNumber;
}

void Game::executePathCardAction(const short &optionNumber, Player& player, std::variant<PathCard, ActionCard> choosedCard)
{
	std::ofstream of("log.log", std::ios::app);
	Logger log(of);
	log.log("Executed path card...", Logger::Level::Info);

	PathCard card = std::get<PathCard>(choosedCard);
	switch (optionNumber)
	{
	case 1:
	{
		bool rotate;
		bool loop = false;
		while (!loop)
		{
			std::cout << "\nDo you want to rotate your card?(1->yes; 0->no)\n";
			std::cin >> rotate;
			while (std::cin.fail()) {
				std::cout << "Enter 1 or 0, please!";
				if (std::cin.fail())
				{
					std::cin.clear();
					std::cin.ignore(256, '\n');
				}
				std::cin >> rotate;
			}
			if (rotate)
			{
				try
				{
					card = card.rotateCard(card);
					std::cout << "\nYour card is: " << card << "\n";
				}
				catch (const char* message)
				{
					std::cout << "\n" << message << "\n";
				}
				std::cout << "\nConfirm?(1->yes; 0->rotate again)\n";
				std::cin >> loop;
				while (std::cin.fail()) {
					std::cout << "Enter 1 or 0, please!";
					if (std::cin.fail())
					{
						std::cin.clear();
						std::cin.ignore(256, '\n');
					}
					std::cin >>loop;
				}
			}
			else
				loop = true;
		}
		bool ok = true;
		while (ok)
		{
			short line, column;
			do
			{
				if (!proceedAction())
				{
					executeAction(player, player.chooseACard());
					ok = false;
					break;
				}
				else
				{
					do
					{
						std::cout << "Choose line(between 0 and " << m_board.getBoardHeight() - 1 << "):";
						std::cin >> line;
						while (std::cin.fail()) {
							std::cout << "Enter number between 0 and " << m_board.getBoardHeight() - 1 << "):";
							if (std::cin.fail())
							{
								std::cin.clear();
								std::cin.ignore(256, '\n');
							}
							std::cin >> line;
						}

					} while (line < 0 || line > m_board.getBoardHeight() - 1);
					do
					{
						std::cout << "Choose column(between 0 and " << m_board.getBoardWidth() - 1 << "):";
						std::cin >> column;
						while (std::cin.fail()) {
							std::cout << "Enter number between 0 and " << m_board.getBoardWidth() - 1 << "):";
							if (std::cin.fail())
							{
								std::cin.clear();
								std::cin.ignore(256, '\n');
							}
							std::cin >> column;
						}
					} while (column<0 || column > m_board.getBoardWidth() - 1);
					if (!cardIsPlaceable(card, line, column))
						std::cout << "\n\tYOU CAN'T PLACE YOUR CARD THERE\n";
					else
					try
					{
						player.placePathCard(card, line, column, m_board);
						player.discard(choosedCard, m_unusedCards.m_cardPack);
						ok = false;
						break;
					}
					catch (const char* message)
					{
						std::cout << "\n" << message << "\n";
					}
				}
			} while (!cardIsPlaceable(card, line, column));
		}
		break;
	}
	case 2:
		player.discard(choosedCard, m_unusedCards.m_cardPack);
		break;
	case 3:
		executeAction(player, player.chooseACard());
		break;
	}
}

void Game::executeActionCardOption(const short &optionNumber, Player& player, std::variant<PathCard, ActionCard> choosedCard)
{
	std::ofstream of("log.log", std::ios::app);
	Logger log(of);
	log.log("Executed action card...", Logger::Level::Info);

	const ActionCard card = std::get<ActionCard>(choosedCard);
	if (optionNumber == 1)
	{
		if (card.getType() == ActionCard::Type::BreakCart || card.getType() == ActionCard::Type::BreakLamp
			|| card.getType() == ActionCard::Type::BreakPickaxe)
		{
			std::vector<Player>::iterator playerToBlock;
			bool ok = true;
			while (ok)
			{
				if (!proceedAction())
				{
					executeAction(player, player.chooseACard());
					ok = false;
					break;
				}
				else
				{
					do
					{
						std::string playerName;
						std::cout << "\nEnter player name(this player will be blocked): ";
						std::cin >> playerName;
						playerToBlock = searchPlayerByName(playerName);
						if (playerToBlock == m_listOfPlayers.end())
							std::cout << "\n\tNO PLAYER WITH THAT NAME FOUND\n";
						if (*playerToBlock == player)
							std::cout << "\n\tYOU CAN'T BLOCK YOURSELF\n";
						else
						{
							if (player.blockPlayer(*playerToBlock, card))
							{
								ok = false;
								player.discard(choosedCard, m_unusedCards.m_cardPack);
								break;
							}
						}
					} while (playerToBlock == m_listOfPlayers.end() || player == *playerToBlock);
				}
			}
		}
		else
			if (card.getType() == ActionCard::Type::FixCart || card.getType() == ActionCard::Type::FixLamp
				|| card.getType() == ActionCard::Type::FixPickaxe || card.getType() == ActionCard::Type::FixLampOrCart
				|| card.getType() == ActionCard::Type::FixPickaxeOrCart || card.getType() == ActionCard::Type::FixPickaxeOrLamp)
			{
				bool ok = true;
				while (ok)
				{
					std::cout << "\n Blocked players:\n";
					for (Player player : m_listOfPlayers)
						if (player.getStatus())
							std::cout << "\t" << player.getName() << "\n";
						std::vector<Player>::iterator playerToUnblock;
						do
						{
							if (!proceedAction())
							{
								executeAction(player, player.chooseACard());
								ok = false;
								break;
							}
							else
							{
								std::string playerName;
								std::cout << "\nEnter player name(this player will be unblocked): ";
								std::cin >> playerName;
								playerToUnblock = searchPlayerByName(playerName);
								if (playerToUnblock == m_listOfPlayers.end())
									std::cout << "\n\tNO PLAYER WITH THAT NAME FOUND\n";
								if (playerToUnblock->getStatus() == false)
									std::cout << "\n\tTHIS PLAYER IS NOT BLOCKED\n";
								else
								{
									if (player.unblockPlayer(*playerToUnblock, card))
									{
										ok = false;
										player.discard(choosedCard, m_unusedCards.m_cardPack);
										break;
									}

								}
							}
						} while (playerToUnblock == m_listOfPlayers.end() && playerToUnblock->getStatus() == false);
				}
			}
			else
				if (card.getType() == ActionCard::Type::View)
				{
					std::string cardType = player.viewDestinationCard(m_board);
					std::cout << "\n\tTHIS CARD CONTAINS: " << cardType.c_str() << "\n\n";
				}
				else
					if (card.getType() == ActionCard::Type::DestroyPath)
					{
						Board::Position position;
						bool loop = true;
						do
						{
							if (!proceedAction())
							{
								executeAction(player, player.chooseACard());
								loop = false;
								break;
							}
							short line, column;

							do
							{
								std::cout << "Choose line(between 0 and " << m_board.getBoardHeight()-1 << "):";
								std::cin >> line;
								while (std::cin.fail()) {
									std::cout << "Enter number between 0 and " << m_board.getBoardHeight() - 1 << "):";
									if (std::cin.fail())
									{
										std::cin.clear();
										std::cin.ignore(256, '\n');
									}
									std::cin >> line;
								}

							} while (line < 0 || line > m_board.getBoardHeight());
							do
							{
								std::cout << "Choose column(between 0 and " << m_board.getBoardWidth()-1 << "):";
								std::cin >> column;
								while (std::cin.fail()) {
									std::cout << "Enter number between 0 and " << m_board.getBoardWidth() - 1 << "):";
									if (std::cin.fail())
									{
										std::cin.clear();
										std::cin.ignore(256, '\n');
									}
									std::cin >> column;
								}
							} while (column<0 || column > m_board.getBoardWidth());
							position = { line, column };
							loop = !player.destroyPathCard(m_board, position);
							if (loop)
								std::cout << "\YOU CAN'T DESTROY THAT\n";
							else
								player.discard(choosedCard, m_unusedCards.m_cardPack);
						} while (loop);
					}
	}
	else
		switch (optionNumber)
		{
		case 2:
			player.discard(choosedCard, m_unusedCards.m_cardPack);
			break;
		case 3:
			executeAction(player, player.chooseACard());
			break;
		}
}

std::vector<Player>::iterator Game::searchPlayerByName(const std::string& name)
{
	for (std::vector<Player>::iterator player = m_listOfPlayers.begin(); player < m_listOfPlayers.end(); ++player)
		if (name.compare(player->getName()) == 0)
			return player;
	return m_listOfPlayers.end();
}

bool Game::cardIsPlaceable(const PathCard& pathCard, const short& line, const short& column)
{
	std::ofstream of("log.log", std::ios::app);
	Logger log(of);
	log.log("Check if the card is placeable...", Logger::Level::Info);

	bool isPlaceable = false;
	m_board[{m_board.getBoardHeight() - 1, m_board.getBoardWidth() / 2}] = PathCard(PathCard::Type::Junction);
	if (!m_board[{ line, column}].has_value())
	{
		std::optional<std::variant<PathCard, BeginCard>> down;
		if (line < m_board.getBoardHeight() - 1)
		{
			Board::Position position = { line + 1, column };
			if (m_board[position].has_value())
				down = *m_board[position];
		}
		std::optional<std::variant<PathCard, BeginCard>> up;
		if (line > 0)
		{
			Board::Position position = { line - 1, column };
			if (m_board[position].has_value())
				up = *m_board[position];
		}
		std::optional<std::variant<PathCard, BeginCard>> left;
		if (column > 0)
		{
			Board::Position position = { line, column - 1 };
			if (m_board[position].has_value())
				left = *m_board[position];
		}
		std::optional<std::variant<PathCard, BeginCard>> right;
		if (column < m_board.getBoardWidth() - 1)
		{
			Board::Position position = { line, column + 1 };
			if (m_board[position].has_value())
				right = *m_board[position];
		}
		if (up.has_value() || down.has_value() || left.has_value() || right.has_value())
			isPlaceable = checkCardNeighbours(pathCard, up, down, left, right);
		m_board[{m_board.getBoardHeight() - 1, m_board.getBoardWidth() / 2}] = BeginCard(BeginCard::Type::Ladder);

		if (up.has_value() && std::holds_alternative<BeginCard>(up.value()))
		{
			if ((right.has_value() && std::holds_alternative<PathCard>(right.value()))
				|| (left.has_value() && std::holds_alternative<PathCard>(left.value()))
				|| (down.has_value() && std::holds_alternative<PathCard>(down.value())))
				return true;
			else
				return false;
		}
		if (left.has_value() && std::holds_alternative<BeginCard>(left.value()))
		{
			if (!(std::get<BeginCard>(left.value()).getType() == BeginCard::Type::Ladder))
				if ((right.has_value() && std::holds_alternative<PathCard>(right.value()))
					|| (down.has_value() && std::holds_alternative<PathCard>(down.value())))
					return true;
				else
					return false;
		}
		if (right.has_value() && std::holds_alternative<BeginCard>(right.value()))
		{
			if (!(std::get<BeginCard>(right.value()).getType() == BeginCard::Type::Ladder))
				if ((left.has_value() && std::holds_alternative<PathCard>(left.value()))
					|| (down.has_value() && std::holds_alternative<PathCard>(down.value())))
					return true;
				else
					return false;
		}
	}
	return isPlaceable;
}

bool Game::checkCardNeighbours(const PathCard& card, const std::optional<std::variant<PathCard, BeginCard>>& up,
	const std::optional<std::variant<PathCard, BeginCard>>& down, const std::optional<std::variant<PathCard, BeginCard>>& left,
	const std::optional<std::variant<PathCard, BeginCard>>& right)const
{
	std::ofstream of("log.log", std::ios::app);
	Logger log(of);
	log.log("Check card neighbours...", Logger::Level::Info);

	if (card.getType() == PathCard::Type::Junction || card.getType() == PathCard::Type::BlindAlleyJunction)
		if (!verifyNeighbours(up, down, left, right, false, false, false, false))
			return false;
	if (card.getType() == PathCard::Type::LeftAndRight || card.getType() == PathCard::Type::BlindAlleyLeftAndRight)
		if (!verifyNeighbours(up, down, left, right, true, true, false, false))
			return false;
	if (card.getType() == PathCard::Type::UpAndDown || card.getType() == PathCard::Type::BlindAlleyUpAndDown)
		if (!verifyNeighbours(up, down, left, right, false, false, true, true))
			return false;
	if (card.getType() == PathCard::Type::DownAndRight || card.getType() == PathCard::Type::BlindAlleyRightAndDown)
		if (!verifyNeighbours(up, down, left, right, true, false, false, true))
			return false;
	if (card.getType() == PathCard::Type::UpAndLeft || card.getType() == PathCard::Type::BlindAlleyLeftAndUp)
		if (!verifyNeighbours(up, down, left, right, false, true, false, true))
			return false;
	if (card.getType() == PathCard::Type::UpAndRight || card.getType() == PathCard::Type::BlindAlleyRightAndUp)
		if (!verifyNeighbours(up, down, left, right, false, true, true, false))
			return false;
	if (card.getType() == PathCard::Type::DownAndLeft || card.getType() == PathCard::Type::BlindAlleyLeftAndDown)
		if (!verifyNeighbours(up, down, left, right, true, false, false, true))
			return false;
	if (card.getType() == PathCard::Type::UpDownAndRight || card.getType() == PathCard::Type::BlindAlleyRightUpAndDown)
		if (!verifyNeighbours(up, down, left, right, false, false, true, false))
			return false;
	if (card.getType() == PathCard::Type::UpDownAndLeft || card.getType() == PathCard::Type::BlindAlleyLeftUpAndDown)
		if (!verifyNeighbours(up, down, left, right, false, false, false, true))
			return false;
	if (card.getType() == PathCard::Type::LeftRightAndUp || card.getType() == PathCard::Type::BlindAlleyLeftRightAndUp)
		if (!verifyNeighbours(up, down, left, right, false, true, false, false))
			return false;
	if (card.getType() == PathCard::Type::LeftRightAndDown || card.getType() == PathCard::Type::BlindAlleyLeftRightAndDown)
		if (!verifyNeighbours(up, down, left, right, true, false, false, false))
			return false;
	if (card.getType() == PathCard::Type::BlindAlleyUp)
		if (!verifyNeighbours(up, down, left, right, false, true, true, true))
			return false;
	if (card.getType() == PathCard::Type::BlindAlleyDown)
		if (!verifyNeighbours(up, down, left, right, true, false, true, true))
			return false;
	if (card.getType() == PathCard::Type::BlindAlleyLeft)
		if (!verifyNeighbours(up, down, left, right, true, true, false, true))
			return false;
	if (card.getType() == PathCard::Type::BlindAlleyRight)
		if (!verifyNeighbours(up, down, left, right, true, true, true, false))
			return false;
	return true;
}

bool Game::verifyNeighbours(const std::optional<std::variant<PathCard, BeginCard>>& upCard,
	const std::optional<std::variant<PathCard, BeginCard>>& downCard, const std::optional<std::variant<PathCard, BeginCard>>& leftCard,
	const std::optional<std::variant<PathCard, BeginCard>>& rightCard, const bool& up, const bool& down, const bool& left, const bool& right)const
{
	if (upCard.has_value())
	{
		PathCard upNeighbour;
		if (std::holds_alternative<PathCard>(upCard.value()))
		{
			upNeighbour = std::get<PathCard>(upCard.value());
			if (upNeighbour.getSouth() == up)
				return false;
		}
	}
	if (downCard.has_value())
	{
		if (std::holds_alternative<PathCard>(downCard.value()))
		{
			const PathCard downNeighbour = std::get<PathCard>(downCard.value());
			if (downNeighbour.getNorth() == down)
				return false;
		}
	}
	if (leftCard.has_value())
	{
		if (std::holds_alternative<PathCard>(leftCard.value()))
		{
			const PathCard leftNeighbour = std::get<PathCard>(leftCard.value());
			if (leftNeighbour.getEast() == left)
				return false;
		}
	}
	if (rightCard.has_value())
	{
		if (std::holds_alternative<PathCard>(rightCard.value()))
		{
			const PathCard rightNeighbour = std::get<PathCard>(rightCard.value());
			if (rightNeighbour.getWest() == right)
				return false;
		}
	}
	return true;
}

bool Game::endOfTurn()
{
	std::ofstream of("log.log", std::ios::app);
	Logger log(of);
	log.log("End of turn...", Logger::Level::Info);

	for (short index = 0; index < m_board.getBoardWidth(); index += 2)
	{
		Board::Position position = { 0, index };
		if (std::holds_alternative<BeginCard>(m_board[position].value()))
			if (std::get<BeginCard>(m_board[position].value()).getType() == BeginCard::Type::Rock)
			{
				Board::Position beginPosition = Board::Position({ m_board.getBoardHeight() - 1, m_board.getBoardWidth() / 2 });
				if (BoardStateChecker::aStarAlgorithm(beginPosition, position, m_board))
					m_board[position] = PathCard(PathCard::Type::Junction);
			}
	}
	BoardStateChecker::State checkBoard;
	checkBoard = BoardStateChecker::Check(m_board);
	if (checkBoard == BoardStateChecker::State::GoldDiggerWin)
		return true;
	auto iterator = m_listOfPlayers.end() - 1;
	if (iterator->getHandPackage().size() == 0)
		return true;
	return false;
}

void Game::distributeGold(const bool &win, Player&winner)
{
	std::ofstream of("log.log", std::ios::app);
	Logger log(of);
	log.log("Distribute gold...", Logger::Level::Info);

	if (win)
	{
		std::cout << "\n\tGOLD DIGGERS WON\n";
		std::vector<GoldCard> cards;
		for (short index = 0; index < m_numberOfPlayers; index++)
		{
			cards.emplace_back(m_listOfGoldCards[0]);
			m_listOfGoldCards.erase(m_listOfGoldCards.begin());
		}
		std::sort(cards.begin(), cards.end());
		if(winner.getRole().getType()== DwarfCard::Type::GoldDigger)
		{
			winner.setScore(winner.getScore() + static_cast<short>(cards.at(0).getType()));
			cards.erase(cards.begin()); 
		}
		while (cards.size() > 0)
		{
			std::vector<Player>::iterator iterator = m_listOfPlayers.begin();
			for (iterator; iterator < m_listOfPlayers.end(); ++iterator)
				if (iterator->getRole().getType() == DwarfCard::Type::GoldDigger)
				{
					iterator->setScore(iterator->getScore() + static_cast<short>(cards.at(0).getType()));
					cards.erase(cards.begin());
					if (cards.size() == 0)
						break;
				}
		}
	}
	else
	{
		std::cout << "\n\tSABOTEURS WON\n";
		std::vector<Player>::iterator iterator = m_listOfPlayers.begin();
		for (iterator; iterator < m_listOfPlayers.end();++iterator)
			if (iterator->getRole().getType() == DwarfCard::Type::Saboteur)
				iterator->setScore(iterator->getScore() + 3);
	}
}


void Game::reinitializePlayers()
{
	std::ofstream of("log.log", std::ios::app);
	Logger log(of);
	log.log("Started new round...", Logger::Level::Info);

	for (auto iterator = m_listOfPlayers.begin(); iterator != m_listOfPlayers.end(); ++iterator)
	{
		iterator->setCardsThatBlockMe(std::vector<ActionCard>());
		iterator->setStatus(false);
		iterator->setHandPackage(std::vector<std::variant<PathCard, ActionCard>>());
	}
}
