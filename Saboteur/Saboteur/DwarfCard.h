#pragma once
#include<iostream>
class DwarfCard
{
public:
	enum class Type : uint8_t
	{
		None,
		GoldDigger,
		Saboteur
	};

	DwarfCard();
	DwarfCard(const Type& type);
	DwarfCard(const DwarfCard &other);
	DwarfCard(DwarfCard &&other);

	~DwarfCard() = default;

	DwarfCard & operator = (const DwarfCard& other);
	DwarfCard & operator = (DwarfCard&& other);

	Type getType() const;

	friend std::ostream& operator << (std::ostream& os, const DwarfCard& actionCard);


private:
	Type m_type : 2;
};

