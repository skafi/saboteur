#pragma once

#include "Player.h"
#include "Board.h"
#include "PathCard.h"
#include "DwarfCard.h"
#include "BeginCard.h"
#include "GoldCard.h"
#include "ActionCard.h"
#include "UnusedCards.h"
#include "BoardStateChecker.h"

#include <vector>
#include <iostream>
#include <algorithm>
#include <random>      
#include <chrono> 
class Game
{
public:

	Game() = default;
	void startGame();

private:
	void generatePlayers();
	void generateDwarfCards();
	void generateBeginCards();
	void generateCardPack();
	void generateGoldCards();
	void distributePlayerRoles();
	void distributeCards();
	void initializeBoard();
	void executeAction(Player&, std::variant<PathCard, ActionCard>);
	bool proceedAction()const;
	short chooseOption()const;
	void executePathCardAction(const short&, Player&, std::variant<PathCard, ActionCard>);
	void executeActionCardOption(const short&, Player&, std::variant<PathCard, ActionCard>);
	std::vector<Player>::iterator searchPlayerByName(const std::string& name);
	bool cardIsPlaceable(const PathCard& pathCard, const short& line, const short& column);
	bool checkCardNeighbours(const PathCard&, const std::optional<std::variant<PathCard, BeginCard>>&,
		const std::optional<std::variant<PathCard, BeginCard>>&, const std::optional<std::variant<PathCard, BeginCard>>&,
		const std::optional<std::variant<PathCard, BeginCard>>&)const;
	bool verifyNeighbours(const std::optional<std::variant<PathCard, BeginCard>>&, const std::optional<std::variant<PathCard, BeginCard>>&,
		const std::optional<std::variant<PathCard, BeginCard>>&, const std::optional<std::variant<PathCard, BeginCard>>&,
		const bool&, const bool&, const bool&, const bool&)const;
	bool endOfTurn();
	void distributeGold(const bool&, Player&);
	void reinitializePlayers();

private:
	short m_numberOfPlayers;
	std::vector<Player> m_listOfPlayers;
	std::vector<DwarfCard> m_listOfDwarfCards;
	std::vector<GoldCard> m_listOfGoldCards;
	std::vector<BeginCard> m_listOfBeginCards;
	UnusedCards m_unusedCards;
	Board m_board;
};

